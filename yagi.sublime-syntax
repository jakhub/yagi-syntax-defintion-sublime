%YAML 1.2
---
# See http://www.sublimetext.com/docs/3/syntax.html
file_extensions:
  - yagi
scope: source.yagi

variables:
  identifier: \b[[:alpha:]_][[:alnum:]_]*\b # upper and lowercase
  number: '\b(-)?[0-9.]+\b'
  assignment_keywords: '=|\+=|-=|\*=|/=|%=|&='
  variable: '\${{identifier}}'
  illegal_declarations: (?:YagiSet|Domain|IntRange|Situation|List|assign|clamp|compileYagiFile|copy|createSituation|fluentNames|new|senseNames|signalNames|signals|startYagiSearch|taskCount|yagiBoolean|yagiRndBoolean|yagiRndInt)


contexts:
  prototype:
    - include: comments
    - include: numbers

  main:
    - include: actions
    - include: facts
    - include: enums
    - include: fluents
    - include: includes
    - include: procedures
    - include: situations
    - include: signal
    - include: sense
    - include: numbers
    # -----
    - match: '\)|\}'
      scope: invalid.illegal.stray-bracket-end.yagi

  expressions:
    - include: controlKeywords
    - include: picks
    - include: choose
    - include: statements
    - include: vars
    - include: strings
    - include: procedureCalls
    # -----
    - include: quantifiers
    - match: '\)|\}'
      scope: invalid.illegal.stray-bracket-end.yagi

  blocks:
    - match: '\{'
      scope: punctuation.section.block.begin.yagi
      push:
        - meta_scope: meta.block.yagi
        - match: '\}'
          scope: punctuation.section.block.end.yagi
          pop: true
        - include: expressions

  statements:
    - include: assign
    - include: braces
    # ----
    - include: strings
    - include: atoms

  braces:
    - match: \(
      scope: punctuation.section.parens.begin.yagi
      push:
        - meta_scope: meta.generic.braces.yagi
        - match: \)
          scope: punctuation.section.parens.end.yagi
          pop: true
        - include: statements
        # statements end in ;

  comments:
    - match: '#'
      scope: punctuation.definition.comment.yagi
      push:
        - meta_scope: comment.line.number-sign.yagi
        - match: \n
          pop: true

  strings:
    - match: '"'
      scope: punctuation.definition.string.begin.yagi
      push: insideString

  insideString:
    - meta_include_prototype: false
    - meta_scope: string.quoted.double.yagi
    - match: '\\.'
      scope: constant.character.escape.yagi
    - match: '"'
      scope: punctuation.definition.string.end.yagi
      pop: true

  includes:
    - match: '\binclude\b'
      scope: keyword.control.import.yagi
    - include: strings

  entityName:
    - include: illegalNames
    - include: genericNames

  genericNames:
    - match: '{{identifier}}'
      scope: meta.entity.name.yagi

  illegalNames:
    - match: \b{{illegal_declarations}}\b
      scope: invalid.illegal.name.yagi

  actions:
    - match: '\baction\b'
      scope: storage.type.action.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.action.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\()'
          set: actionDefinitionParams

  actionDefinitionParams:
    - meta_content_scope: meta.action.yagi
    - match: '(?=\()'
      set:
        - match: \(
          scope: meta.action-parameters.yagi punctuation.section.parens.begin.yagi
          set:
            - meta_content_scope: meta.action-parameters.yagi 
            - match : \)
              scope: punctuation.section.parens.end.yagi
              set: actionDefinitionBody
            - include: parameters

  actionDefinitionBody:
    - meta_content_scope: meta.action.yagi
    - match: '\{'
      scope: meta.block.yagi punctuation.section.block.begin.yagi
      set:
        - meta_content_scope: meta.action.yagi meta.block.yagi
        - match: '\}'
          scope: meta.action.yagi punctuation.section.block.end.yagi meta.block.yagi
          pop: true
        - match: '(\bposs|\bdo)\s*(?=\{)?'
          captures:
            1: storage.modifier.action-poss.yagi
            2: storage.modifier.action-do.yagi
        - include: braces
        - include: blocks
        - include: signal

  enums:
    - match: '\benum\b'
      scope: storage.type.enum.yagi
      set:
        - match: '(?={{identifier}})'
          set:
            - meta_content_scope: entity.name.enum.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\{)'
          pop: true
  
  facts:
    - match: '\bfact\b'
      scope: storage.type.fact.yagi
      set:
        - match: '(?={{identifier}})'
          set:
            - meta_content_scope: entity.name.fact.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\[)'
          pop: true
    - include: domains
    #- include: sets

  fluents:
    - match: '\bfluent\b'
      scope: storage.type.fluent.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.fluent.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\[)'
          pop: true
    - include: domains

  procedures:
    - match: '\bproc\b'
      scope: storage.type.procedures.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.procedure.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\()'
          set: procedureDefinitionParams
    - include: blocks

  procedureDefinitionParams:
    - meta_content_scope: meta.procedure.yagi
    - match: '(?=\()'
      set:
        - match: \(
          scope: meta.procedure-parameters.yagi punctuation.section.parens.begin.yagi
          set:
            - meta_content_scope: meta.procedure-parameters.yagi 
            - match : \)
              scope: punctuation.section.parens.end.yagi
            - match: '(?=\{)'
              pop: true
            - include: parameters

  procedureCalls:
    - match: '(?!\s*while)(?!\s*if)(\bsearch\b)?\s*({{identifier}})\s*(?=\()'
      captures:
        1: storage.modifier.procedure.search.yagi
        2: variable.other.procedure.yagi 
    - include: entityCallArgs

  situations:
    - match: '\bsituation\b'
      scope: storage.type.situation.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.situation.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\{)'
          pop: true
    - include: blocks

  entityDefinitionParams:
    - meta_content_scope: meta.entity.yagi
    - match: \(
      scope: meta.entity.parameters.yagi punctuation.section.parens.begin.yagi
      push:
        - meta_content_scope: meta.entity.parameters.yagi 
        - match : \)
          scope: punctuation.section.parens.end.yagi
          pop: true
        - include: parameters

  entityCallArgs:
    - meta_content_scope: meta.entity.yagi
    - match: \(
      scope: meta.entity.arguments.yagi punctuation.section.parens.begin.yagi
      push:
        - meta_content_scope: meta.entity.arguments.yagi 
        - match : \)
          scope: punctuation.section.parens.end.yagi
          pop: true
        - include: statements

  signal:
    - match: '\bsignal\b'
      scope: storage.type.signal.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.signal.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\()'
          pop: true
    - include: entityDefinitionParams

  sense:
    - match: '\bsense\b'
      scope: storage.type.sense-def.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.sense.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\()'
          pop: true
        - include: types
    - include: entityDefinitionParams

  senseCalls:
    - match: '\bsense\b'
      scope: storage.type.sense-call.yagi
      push:
        - match: '(?={{identifier}})'
          push:
            - meta_content_scope: entity.name.sense.yagi
            - include: entityName
            - match: ''
              pop: true
        - match: '(?=\()'
          pop: true
    - include: entityCallArgs

  picks:
    - match: '\bpick\b'
      scope: keyword.other.pick.yagi
    - include: tupleAssignments
    - include: inModifiers

  inModifiers:
    - match: '\bin\b'
      scope: keyword.other.modifier.in.yagi
      push: quantifierSet
    - include: suchModifiers

  quantifierSet:
    - include: domains
    - include: atoms
    - include: operators
    - match: '{{identifier}}'
      scope: variable.other.quantifier-set.yagi
      pop: true

  suchModifiers:
    - match: '\bsuch\b'
      scope: keyword.other.modifier.such.yagi
    - match: '(?=\{)'
      set: blocks

  types:
    - match: '\bfloat|string|boolean\b'
      scope: storage.type.primitive.yagi
    - include: intTypes
    - include: setTypes
    - include: domains
    - match: '{{identifier}}'
      scope: storage.type.user-defined.yagi

  intTypes:
    - match: '\b(int)(\[(?={{number}}?)(:)(?={{number}})\])?'
      captures:
        1: storage.type.int.yagi
        3: punctuation.separator.sequence.yagi

  domains:
    - match: '\['
      scope: punctuation.section.brackets.begin.yagi
      push:
        - meta_content_scope: meta.domain.yagi
        - match: '\]'
          scope: punctuation.section.brackets.end.yagi
          pop: true
        - include: types

  setTypes: 
    - match: '\{'
      scope: punctuation.section.braces.begin.yagi
      push: 
        - meta_content_scope: meta.set-type.yagi
        - match: '\}'
          scope: punctuation.section.braces.end.yagi
          pop: true
        - include: types
  
  any:
    - match: '_'
      scope: variable.other.constant.any.yagi

  vars:
    - match: '\$(?={{identifier}})'
      scope: punctuation.definition.variable.yagi
      push: 
        - match: '{{identifier}}'
          scope: variable.other.yagi
        - match: ''
          pop: true

  params:
    - match: '\$(?={{identifier}})'
      scope: punctuation.definition.variable.yagi
      push: 
        - match: '{{identifier}}'
          scope: variable.parameter.yagi
        - match: ''
          pop: true

  parameters:
    - include: types
    - include: params

  quantifiers:
    - match: '\bfor|exists\b'
      scope: keyword.other.quantifier.yagi
    - include: operators
    - include: tupleAssignments
    - include: suchModifiers
    - include: inModifiers

  atoms:
    - include: braces
    - include: strings
    - include: vars
    - include: any
    - include: tuples
    - include: senseCalls
    - include: setAccesses
    - include: enumAccesses
    #- include: sets

  sets:
    - match: '\{'
      scope: punctuation.section.braces.begin.yagi
      push:
        - meta_content_scope: meta.set.yagi
        - match: '\}'
          scope: punctuation.section.braces.end.yagi
          pop: true
        - include: domains
        - include: statements

  tuples:
    - match: '\['
      scope: punctuation.section.brackets.begin.yagi
      push:
        - meta_content_scope: meta.tuple.yagi
        - match: '\]'
          scope: punctuation.section.brackets.end.yagi
          pop: true
        - include: statements

  setAccesses:
    - match: '({{variable}}|{{identifier}})(\[)'
      captures:
        1: variable.other.set.access.yagi
        2: punctuation.section.brackets.begin.yagi
      push:
        - meta_content_scope: meta.set-access.yagi
        - match: '\]'
          scope: punctuation.section.brackets.end.yagi
          pop: true
        - include: statements

  enumAccesses:
    - match: '{{identifier}}(\.){{identifier}}'
      scope: variable.other.enum.access.yagi
      captures:
        1: punctuation.accessor.dot.yagi
# ---

  constants:
    - match: \btrue|false\b
      scope: constant.language.yagi

  choose:
    - match: '\bchoose\b'
      scope: constant.other.choose.yagi
    - include: orModifier

  orModifier:
    - match: '\bor\b'
      scope: constant.other.choose-or.yagi
    - include: blocks
      set: orModifier

  operators:
    - match: <\=|>\=|\=\=|<|>|\!\=
      scope: keyword.operator.comparison.yagi
    - match: \+|\-|\*|/|%|!
      scope: keyword.operator.arithmetic.yagi
    - match: '&&|\|\||->'
      scope: keyword.operator.logical.yagi
    - match: '\b(and|or|implies)\b'
      scope: keyword.operator.word.yagi

  assign:
    - match: '({{identifier}})\s*(?={{assignment_keywords}})'
      captures:
        1: variable.other.assignment.yagi
    - include: tupleAssignments
    - include: vars
    - include: assignments
    - include: quantifiers

  assignments:
    - match: '\+=|-=|\*=|/=|%=|&='
      scope: keyword.operator.assignment.augmented.yagi
    - match: '=(?!=)'
      scope: keyword.operator.assignment.yagi

  tupleAssignments:
    - match: '\['
      scope: punctuation.section.brackets.begin.yagi
      push:
        - meta_scope: meta.tuple-assignment.yagi
        - match: '\]'
          scope: punctuation.section.brackets.end.yagi
          pop: true
        - include: vars
        - include: any
        # ----
        - include: intTypes
        - include: enumAccesses

  controlKeywords: 
    - match: '\b(if|else|while)\b'
      scope: keyword.control.yagi
    - include: braces
    - match: '(?=\{)'
    - include: blocks

  numbers:
    - match: \b(0x)[0-9,A-F]+\b
      scope: constant.numeric.integer.hex.yagi
      captures:
        1: punctuation.definition.numeric.hex.yagi
    - match: \b(0b)[01]+\b
      scope: constant.numeric.integer.binary.yagi
      captures:
        1: punctuation.definition.numeric.binary.yagi
    - match: \b[-]?[0-9]+(\.)[0-9]+\b
      scope: constant.numeric.float.yagi
      captures:
        2: punctuation.separator.decimal.yagi
    - match: \b0|[1-9][0-9]*|-[1-9][0-9]*\b
      scope: constant.numeric.integer.dec.yagi