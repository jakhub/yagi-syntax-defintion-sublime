# YAGI Syntax Defintion Sublime

Simple and not very well tested syntax definition for sublime text 3 for YAGI.

Should work for most color schemes.

Might be incomplete.

place syntax file in `~/.config/sublime-text-3/Packages/User`.

# Example

example using Mariana color-scheme: ![](sublime_robot_yagi.png)